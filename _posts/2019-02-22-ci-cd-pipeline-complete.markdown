---
layout: post
title:  "CI/CD v1.1 Complete!"
date:   2019-02-22 11:33:00 -0800
categories: Devops
---

I've completed v1.1 of my CI/CD process for this blog. It's fairly straight forward, clone to your local machine, test using "jekyll build && jekyll serve". When it looks good, push to develop. If it passes all tests, look at the online version at [https://cworobetz.gitlab.io/worobetz-blog/](https://cworobetz.gitlab.io/worobetz-blog/) . If that looks good too, you must create a merge request in the GitLab repository at [https://gitlab.com/cworobetz/worobetz-blog](https://gitlab.com/cworobetz/worobetz-blog) for the develop branch to go into the master branch. Once merged, the CI/CD process (defined in the root of the repository in .gitlab-ci.yml) will detect it's building for the master branch, select the production runner (In this case, my personal webserver), and execute a script that deploy the static webpages to the appropriate www directory.

Check it out!
