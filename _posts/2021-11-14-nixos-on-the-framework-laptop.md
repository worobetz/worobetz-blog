---
layout: post
title:  "NixOS on the Framework Laptop"
date:   2021-11-14 17:00:00 -0800
categories: hardware linux nixos
---

## NixOS on the Framework Laptop - A review + guide

I recently ordered a [Framework Laptop](https://frame.work). I had been using a Surface Pro 5 before which was growing old and crusty at this point, and I was in the market for a new laptop.

The moment I first saw Framework on HackerNews (or Tildes?) I instantly knew we had a winner, provided Framework (the company) actually pulled it off and shipped units on time. Building your own PC is the ultimate way to own your computer, and no laptop manufacturer had stood up yet make that their priority. The closest I've heard in that regard is Lenovo Thinkpads, which weren't ideal for other reasons.

## The Hardware

I purchased Framework Laptop DIY Edition, batch 4, with the following configuration

* CPU: i7-1165G7 (12M Cache, up to 4.70 GHz)
* Storage: 1TB - WD_BLACK SN850 NVMe
* RAM: 32GB (1 x 32GB) DDR4-32001x32GB DDR4 Memory
* WiFi: Intel® Wi-Fi 6E AX210 No vPro
* Expansion cards: 2x USB-C, 1x USB-A, 1x HDMI

I wanted to be able to run a semi-intensive server infrastructure on my laptop - Multiple VMs, Kubernetes clusters, etc. So having large amounts of RAM and a decent CPU was essential.

## The Software

I had read a few "NixOS on Framework" posts before me[[1]](https://kvark.github.io/linux/framework/2021/10/17/framework-nixos.html) [[2]](https://grahamc.com/blog/nixos-on-framework) and after a bit of research, NixOS seemed to perfectly suit my needs:

* Incredibly stable - It's hard to brick your system
* Declarative - You can store your entire system configuration in a file, or more as needed for organization
* Large software repository - NixPkgs has most software prepackaged for you

### Installing

After assembling my laptop, I got right into installing NixOS. After some trial and error with getting my laptop to boot off of USB (Secure Boot has to be disabled), I was into the installer environment.

I quickly found that the default NixOS installer ISO was unsuitable - the installer didn't include drivers required to use the WiFi card that my laptop came with. Previous blog posts about this had warned me about this, with two potential options - install using the NixOS unstable ISO, which includes the latest kernel (and necessary drivers), or upgrade the installer environments kernel - which required internet. I actually ended up choosing a third option - generate my own ISO. NixOS has a handy tool for generating your own ISOs off of Nix expressions. The below is the Nix expression I used to generate an ISO I could boot off of that would support WiFi.

```nix
## This module defines a small NixOS installation CD.  It does not
## contain any graphical stuff.
{config, pkgs, ...}:
{
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>

    # Provide an initial copy of the NixOS channel so that the user
    # doesn't need to run "nix-channel --update" first.
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

  boot.kernelPackages = pkgs.linuxPackages_latest;
}
```

You can then build this image using:

```bash
nix-build '<nixpkgs/nixos>' -A config.system.build.isoImage -I nixos-config=iso.nix
```

Install the resulting ISO file to a USB using e.g. Ventoy, and you're good to go!

Once inside the installer environment, you can connect to WiFi using:

```bash
wpa_supplicant -B -i wlan0 -c <(wpa_passphrase 'SSID' 'password')
```

At which point, you can continue with the installation as per NixOS instructions.

## My evolving setup

Currently, I'm using Gnome on NixOS's unstable channel. So far, the experience has been much much better than my other experiences on Linux desktops

* I never feel worried I'll accidentally brick my OS. NixOS makes it entirely too easy to rollback and forward your entire OS using a single command, and you never get put into a weird state where things are broken. At least, I haven't gotten into any jams as such.
* Battery life is excellent. I'd estimate I can get 7-9 hours of battery life on a full charge
* Sleep mode works as expected, saving your battery when not in use
* The fingerprint scanner works as advertised
* NixOS has a huge selection of packages available. I'm always able to get the package I want installed from the default repository
* Even if a command is not packaged, docker is always available for a quick ephemeral environment. In fact, I plan to do most of my development work in docker (or podman) containers
* Steam works, and several games I've tried "Just work." Steam streaming works even better than I expected over WiFi. I was streaming Fallout: New Vegas from my PC, and using a wireless controller, and the experience was phenomenal.
* The wireless adapter supports monitor mode. Running Kali linux in a container and being able to hone my infosec skills has been fun

## Future plans

Currently, I have a very basic NixOS installation: A boot partition, a root partition on ext4, and a swap partition. That's no fun! I have plans to reinstall NixOS on an encrypted ZFS root partition with compression enabled. I value my hardware's security and what better way to do that than with full disk encryption (other than, of course, VeraCrypt). I'd also like to switch my NixOS installation to be using flakes, but I haven't quite figured out the finer details of those yet - that may be a topic for another blog post.
