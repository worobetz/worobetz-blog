---
layout: post
title:  "Jekyll pipeline built!"
date:   2019-02-12 22:52:00 -0800
categories: meta
---

I picked up Jekyll today to build this blog website. I decided to use GitLab's integrated CI service, as of right now the pipeline is super simple.

If you wish to take a peek at my blog repository, take a look [here](https://gitlab.com/cworobetz/worobetz-blog).
