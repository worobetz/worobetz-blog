---
layout: post
title:  "What I Like About Different Programming Languages"
date:   2021-05-16 00:15:00 -0800
categories: programming software
---

I've been in IT / DevOps now for about 6 years, and in that time I've worked with a multitude of different languages. In high school, I learned visual basic. At BCIT, I learned to use Java, C#, Javascript, and PHP. Outside of school, I took it upon myself to learn Powershell, Python, Golang, and now I'm starting to dip my toes into the water of Rust. In this post, I'll talk a little about a few of my favorite languages and why exactly I choose to use them.

## Python

Python is a fantastic general purpose language. It's not all too far removed from REPL languages, as it's interpreted - there's no need to compile your source code. Dependency management is a bit of a manual process, even using pip, but it isn't too much of a hassle. It's also extremely easy to write - the code at times can resemble psuedocode.

There are some cons to the language though - Python code, in large codebases, can get messy very quickly. There's a lot of ways to do things, and naturally, people disagree about the correct way to do it. It's also pretty slow. It's fine for things that aren't sensitive to speed, but it is noticable.

## Golang

Probably my "favorite" language, Golang came out of Google a few years ago and it has really taken off. Selling poins of Golang include its fantastic tooling (code formatting, testing, compiling), decent dependency management, and speed. Golang in most tests is slightly slower than Rust and C, but not by much - perhaps 30%, depending on a lot of factors.

Go code tends to be well formatted (the compiler has a formatting tool built in), and Go code is easy to read compared to some languages. Not as easy as Python, but easy.

One of my favorite bits about Golang is its "idiomatic-ness" - code tends to be written sameish, even among different developers. This makes it easy to jump into a codebase and start looking around quickly. It also has terrific cross platform support; it's very easy to compile a binary for Windows, Linux, or OSX, assuming you only use standard libraries / compatible libraries. 

## Rust

Rust is the newest language in my toolbelt. I'm still in the process of learning it, so take my opinion with a grain of salt. Rust is the language de jour of systems engineers, loved for its memory safety and excellent tooling. In my opinion, Rust is quite a difficult language to learn - There is a lot of syntax to learn, and it isn't always clear what you're looking at. 

Rust is a fantastic choice of programming language for many pieces of software, particularly software that you don't want to be buggy. The compiler is seriously next-level when it comes to pointing out syntax errors in your code, and what you can do to fix it. And there will be syntax errors in your code - the language is extremely strict that you write your code correctly, as the language is memory safe. The result of finding and fixing all of these errors earlier in your code is that your code tends to be pretty bug-free once you've made it compile. 

Another plus of Rust is the tooling - Cargo for Rust is the package manager, and it's extremely easy to use. On top of that, Rust is very fast - almost comparable to C.
