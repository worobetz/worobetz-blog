---
layout: post
title:  "A Timeless Cookbook: Recipes from the year 1896"
date:   2019-06-27 01:00:00 -0800 
categories: cooking recipes history cookery
---

Yesterday, I stumbled upon one of the coolest collection of recipes I've ever seen. Simply titled, ["The cook book" by Oscar Tschirky](https://archive.org/details/tchirkycookbook00tschrich) is a collection of thousands of recipes published in 1896. 

According to his [wikipedia page](https://en.wikipedia.org/wiki/Oscar_Tschirky), "Oscar Tschirky (1866 - 1950) was a Swiss-American restauranteur who was maître d'hôtel of Delmonico's Restaurant and subsequently the Waldorf-Astoria Hotel in Manhattan, New York, United States. He was widely known as "Oscar of the Waldorf" and published a large cookbook." 
Oscar is credited with having invented the [Waldorf salad](https://en.wikipedia.org/wiki/Waldorf_salad), a salad so famous it has its own wikipedia page!

Like the cookbook's strikingly simple name, the recipes contained within are short and to the point. While a lot of the recipes use "advanced" cooking techniques, Oscar Tschirky managed to condense them all to a size easily digestable in a few short sentences. It's written for an audience that already knows basic culinary principals, and as such it cuts straight to the point. A lot of the recipes are ones that you might have heard about at some point, but have forgotten exist. Astute readers with the desire to learn will quickly pick up important culinary techniques as they work through the recipes and examine the similaries and differences between dishes. Readers who are looking at it as a source for inspiration will be overwhelmed by choice: It shows 43 different recipes for potatoes alone at one point, each using different cooking methods and appropriate for different purposes, not just with subtle variations between each.

Give it a read at the link below!

[Link](https://archive.org/details/tchirkycookbook00tschrich)
