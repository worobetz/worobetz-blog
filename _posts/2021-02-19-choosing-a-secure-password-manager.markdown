--- 
layout: post 
title: "Choosing a secure password manager" 
date: 2020-02-11 10:32:00 -0800 
categories: software passwords security opensource 
---

I frequent many internet forums, and I often encounter questions about which
password manager to use. "Should I use LastPass? 1Password? Google Chrome's
password manager?"

The answer to all of those is a resounding no. You should not be using any of
those password systems. They're inherently insecure, and should be avoided at
all costs.  The reason? They're all closed source software.

There's a lot of different types of software, but for the purposes of this post,
there are two types: Open-source software and closed source software. Open
source software means that the source code is provided to you - given you have
basic software development experience, you're able to compile the raw,
human-readable code into a binary executable and use it to do things. The
important thing here is that you know exactly what code has gone into that
binary. The binary itself is unreadable (as a human), but that's OK because
you've compiled it yourself so you know what's in it.

With closed source software, you are provided the only the binary. You do not
know what software is within this binary, end of story.

Obviously, in the case of password managers, not knowing what software is
running within the password manager's binary is bad. It could be siphoning off
your passwords to who knows where. This is why it's imperative that when
choosing your password manager, you ensure that it's completely open source
software. Ideally, free open source software (Free as in Freedom!).

So, what should you use? I've made two recommendations below, one for personal
use, and one for shared / business use.

### Personal Use

I've made a lot of password manager recommendations in the past, and I still
stand behind KeePass! I've been using this set up for almost 7 years.
Specifically:

* KeePassXC - for the desktop app. Available at <https://keepassxc.org> (Free, open
source software)
* KeePassXC-Browser - An extension for autofill in your browser
of choice, on the chrome / firefox extension stores (Free, open source software)
* Keepass2Android - An android app for opening KeePass databases, on the play
store (Free, open source software)
* Google Drive Sync (Or any file syncing program of your choice, it doesn't
affect the security) - For syncing the encrypted password database file to
all of your devices. This is secure, because the database file is encrypted.
(Gratis, but closed source software)

This complete solution provides you the following crucial features, ones that I use
and appreciate daily:

* Password syncing to all of your devices
* Strong, verifiably secure encryption of your passwords TOTP, both the standard
 version and Steam version
* Autofill for usernames / password in your browser

 And it also boasts other useful features:

* Support for hardware keys and key files, in addition to a password, for your
password database (I use a key file that I physically copy, offline, to my
devices)
* CLI support SSH-agent integration - Automatically add your SSH keys to
SSH-agent when you unlock your password database. This is a godsend.
* Dark mode (KeePassXC)
* Completely free, open source software - Nobody is going to pull the
rug out from under you, and insist you pay them to continue using the software.
KeePassXC, Keepass2Android, and the KeePassXC-browser extensions are completely
free, will always be free, and are here to stay.
* No need to host your own servers - suitable for the tech-inexperienced

This set up has been so useful for me I use it for things outside of just
passwords. For example, I store my credit cards, clothing size measurements,
SIN, driver's license information, and other useful information in my password
database.

### Shared / Business Use

I recommend BitWarden, via the server [bitwarden_rs](https://github.com/dani-garcia/bitwarden_rs). You access bitwarden via a web interface.

BitWarden is a free, open-source password manager suitable for group or shared use. While you can use the official version of the server from BitWarden Inc., it requires a license to use paid features such as directory connectors, login with SSO, user groups, and more. It's still open source, but locked behind a paid license (Technically you could edit and compile the source code yourself to remove these restrictions, but their official server is still resource heavy).

The bitwarden_rs project enables most of these enterprise features for free. In addition to that, it's written in the Rust programming language, which is an interesting language as it guarantees memory safety - improper memory safety being the cause of a very large portion of security bugs. It's also quite lightweight, bitwarden_rs should have no problem running on a raspberry pi.
