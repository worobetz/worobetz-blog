---
layout: post
title:  "Storing recipes in git"
date:   2019-02-22 09:53:00 -0800
categories: Ideas
---

As a cooking and computing enthusiast, it's very rare that my two hobbies intersect: Cooking (for my purposes) is an inherently offline process involving liquids and gasses and heat and sweat, while computing is a very logical activity largely concerned with the manipulation of data. A point I've discovered where these two activities intersect is recipes: Recipes are simply text files that need to be maintained. 

Recipes can be improved, however, sometimes changes can have unintended side effects and need to be reverted. Recipes are subject to personal opinion: What you like in your recipe may not necessarily be what I like in my recipe. And recipes are subject to collective opinion: Some recipe changes are so game changing, so widely respected that almost everyone will end up adopting it. These are all things that git is well suited to handle, and as such I decided to manage my favorite recipes using git.

My workflow is as such: I've defined a template that recipes can and should use. It encourages the use of good practices, such as using weights instead of volume measurements, and using metric instead of imperial. It's important to note that these are the standards, but are not enforced: Recipes are very human in nature, and should be defined "loosely" as opposed to defining very strict recipe syntax.

After copying the template to a new file, for example, waffles.md, I go in and write the recipe. I try to include the original source when possible. When all's said and done, I stage the recipe for commiting, commit the recipe, and push it to my repositories master branch. The repository is public and can be viewed online, a link is below. Not that it has happened before, but if someone improves upon my recipes, they can submit a pull request. If I find their idea improves my recipe, I'd merge that pull request. If someone likes my recipes, they can clone my repository and add recipes of their own, ideally making pull requests to my original repository for their new recipes.

A direct link to my git repository can be found [here](https://gitlab.com/cworobetz/recipes).
