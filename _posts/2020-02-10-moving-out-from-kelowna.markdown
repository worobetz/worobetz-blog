---
layout: post
title:  "Moving out from Kelowna and other exciting things"
date:   2020-02-11 10:32:00 -0800
categories: kelowna life
---

My time in Kelowna is at an end! I first moved here shortly after graduating BCIT in 2016 with my girlfriend at the time, job in hand. Over the next three years, I experienced some of the best the Okanagan has to offer - food, people, and places. In celebration of that, here's my take on the best food in Kelowna.

**Best food**
 * Sushi: Ume foods. Ume is a small Japanese eatery serving wholesome food at an affordable price. There's a lot of sushi restaurants in Kelowna, and I've been to nearly all of them, but none of them approach the consistency and deliciousness of Ume.
 * Bubble tea: Formosa Tea House. Bubble tea isn't as popular in Kelowna as it is in the lower mainland, but Kelowna still has tasty bubbles to be found at Formosa.
 * Pizza: Bellissimo! Pizza.  Bellissimo is a tiny pizzaria found in South Kelowna. I'm not entirely sure how it still operates, but the lone employee there dishing out pizzas is totally on his game. I fondly remember walking there with my friends and enjoying a hot pizza, outside in the park, on a cool summer night.
 * Burgers: Bin 4. Bin 4 is a relative recent addition to Kelowna, with other restaurants in the Vancouver / Island area. Their burgers are creative yet holistic, incorporating interesting ingredients in a delicious and nonthreating manner. Their beer selection, sides, and dips are also incredibly tasty.
 * Pho: Pho Soc Trang Vietnamese Cuisine. The only pho place I visited in Kelowna (what was I thinking?) didn't disappoint
 * Italian: L'Isola Bella Bistro. Damn delicious Italian food in a homely, cozy restaurant. What more could you want? Their lunch paninis (served with salad and veggie straws) are to die for, and make a perfect to-go lunch.
 * Western fare: Okanagan Street Food. Hands down my favorite restaurant in Kelowna, Okanagan street food is run by chef Neil Schroeter and a small crew of two other cooks, from what I can tell. They sell _properly_ made stocks and soup for home use, and endeavour to buy local, fresh, seasonal products. And that's not even the best part: Their poutine (with added bacon) is simply the best in BC, perhaps Canada. Fried in duck fat, nothing even comes close. You can enjoy the fries with their house-made blackberry ketchup (present on every table) if you're so inclined. If you're going to go anywhere to eat in the Okanagan, this is it. Their breakfast is also simply incredible served with a cup of the Okanagan's Cherry Hill coffee.
 * Ukrainian: Cecil's Pierogies. The restaurant itself is a little out of the 90's, but don't let that deter you. Speaking as a former cook, and growing up in a Ukrainian / Saskatchewan household with a chef as a father, these are simply the best pierogies I've _ever_ had. Their sauce is simply to die for. The cashier is also incredibly friendly and makes everybody who walks through the door feel welcome, enhancing the whole experience.
 * Mexican: Latin Fiesta. Located in Rutland, Latin Fiesta makes some great latin food, including burritos and tacos. They also have an excellent drink and hot sauce selection!
 * Honorable Mention: The Bunkhouse in South Kelowna. Their cozy atmosphere (with a real wood fireplace!), delicious breakfast and hearty dinners have earned themselves a spot on this list.
