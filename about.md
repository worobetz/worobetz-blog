---
layout: page
title: About
permalink: /about/
---

Occasionally, I'll feel like writing blog posts here.

You can visit my main site [here](https://worobetz.ca).
